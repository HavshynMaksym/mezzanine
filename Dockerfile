FROM python:3.8

RUN mkdir -p /src/project/commands
WORKDIR /src/project/

COPY ./myproject ./
COPY ./commands/ ./commands/
COPY ./requirements.txt ./

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

#CMD ["python", "manage.py", "runserver", "8000"]
CMD ["bash"]
